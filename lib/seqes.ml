(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*                 Raphaël Proust                                         *)
(*                                                                        *)
(*   Copyright 2022 Nomadic Labs                                          *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(** Signatures for all the functors exported afterwards. The documentation of
    this module also introduces some general concepts of the library. It is a
    recommended reading for all users of the library. *)
module Sigs = struct
  include Sigs1
  include Sigs2
end

(** Functors to generate monadic traversors over the {!Stdlib.Seq.t} type. *)
module Standard = Standard

(** Functors to generate specialised monadic variants of the {!Stdlib.Seq.t}
    type along with all the traversor and transformer functions. *)
module Monadic = struct
  include Monadic1
  include Monadic2
end

(**/**)
(* Useful for tests, possibly also for other uses? *)
module Identity1 = Identity1
module Identity2 = Identity2
