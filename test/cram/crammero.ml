module SeqO = Seqes.Monadic.Make1(struct
  type 'a t = 'a option
  let return x = Some x
  let bind x f = Option.bind x f
end)

let wrap name f =
  Format.printf "%s: "name;
  match f () with
  | Some () -> Format.printf "DONE\n"
  | None -> Format.printf "INTERRUPTED\n"

let () =
  let limit = int_of_string Sys.argv.(1) in
  wrap "mod3" (fun () ->
    SeqO.ints 0
    |> SeqO.take 50
    |> SeqO.map (fun x -> x * 2)
    |> SeqO.M.filter (fun x ->
        if x > limit then
          None
        else
          Some (x mod 3 = 0))
    |> SeqO.iter (Format.printf "%d ")
  );
  wrap "negative" (fun () ->
    SeqO.ints 0
    |> SeqO.take 40
    |> SeqO.M.filter_map (fun x ->
        if x > limit then
          None (* hard interrupt *)
        else if x mod 2 = 0 then
          Some None (* skip *)
        else
          Some (Some (-x)) (* use *)
       )
    |> SeqO.iter (Format.printf "%d ")
  );
  wrap "fibonnaci" (fun () ->
    let f =
      SeqO.unfold
        (fun (a, b) ->
          let c = a + b in
          Some (c, (b, c)))
        (0, 1)
      |> SeqO.memoize
    in
    SeqO.ints 0
    |> SeqO.take 10
    |> SeqO.map (fun d -> SeqO.take d f)
    |> SeqO.transpose
    |> SeqO.concat
    |> SeqO.M.iteri (fun i v ->
        if i > limit then
          None
        else
          Some (Format.printf "%d " v))
  );
  ()

(* This cram test is mostly intended as a regression test. It doesn't do
   anything interesting, but it does exercise some basic functions of Seqes. *)
