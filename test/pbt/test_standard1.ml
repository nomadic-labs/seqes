(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*                 Raphaël Proust                                         *)
(*                                                                        *)
(*   Copyright 2022 Nomadic Labs                                          *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* Tests Seqstd against Stdlib *)

(* We test Seqstd by feeding it the Identity Monad: the resulting module should be
   indistinguishable from [Stdlib.Seq] (modulo the type being new) *)
module SeqM = Seqes.Standard.Make1(Seqes.Identity1)

module TestsSeqMTraversors = Helpers.MakeTestSuites(struct
  include Seq
  include SeqM (* overwrite traversors *)
  let of_seq s = s
end)

let () =
  Alcotest.run
  "Seqes.Standard.Make1"
  [("Identity", TestsSeqMTraversors.traversors)]

