(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*                 Raphaël Proust                                         *)
(*                                                                        *)
(*   Copyright 2022 Nomadic Labs                                          *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* The [Seq] module shows how to build extensions to the [Stdlib.Seq] module to
   use a monad in the traversal. *)
module Seq
: sig

  (* The whole of the Seq module from Stdlib *)
  include module type of struct include Stdlib.Seq end

  (* Additional Lwt traversors *)
  module S : Seqes.Sigs.SEQMON1TRAVERSORS
    with type 'a mon := 'a Lwt.t
    with type 'a callermon := 'a Lwt.t
    with type 'a t := 'a Stdlib.Seq.t

end
= struct
  include Stdlib.Seq
  module S = Seqes.Standard.Make1(Lwt)
end

let () =
  Lwt_main.run begin
    List.to_seq ["a";"b";"c";"\n"]
    |> Seq.S.iter Lwt_io.print
  end

(* The [SeqS] module shows how to
   - build custom [Stdlib.Seq]-like modules with a baked-in I/O monad,
   - provide additional monadic functions, and
   - provide additional function for a different monad altogether.
*)
module SeqS
: sig

  (* Similar to [Stdlib.Seq] but with [Lwt] baked into the sequence type. *)
  include Seqes.Sigs.SEQMON1ALL
    with type 'a mon := 'a Lwt.t

  (* Additional Lwt helpers. E.g., [S.map : ('a -> 'b Lwt.t) -> 'a t -> 'b t]. *)
  module S :
    Seqes.Sigs.SEQMON1TRANSFORMERS
      with type 'a mon := 'a Lwt.t
      with type 'a callermon := 'a Lwt.t
      with type 'a t := 'a t

  (* Additional result helpers. E.g.,
     [E.iter : ('a -> (unit, 'e) result) -> 'a t -> ('a, 'e) result Lwt.t] *)
  module E :
    Seqes.Sigs.SEQMON2TRAVERSORS
      with type ('a, 'e) mon := ('a, 'e) result Lwt.t
      with type ('a, 'e) callermon := ('a, 'e) result
      with type ('a, 'e) t := 'a t

  (* Additional Lwt-result helpers. E.g.,
     [E.iter : ('a -> (unit, 'e) result Lwt.t) -> 'a t -> ('a, 'e) result Lwt.t] *)
  module ES :
    Seqes.Sigs.SEQMON2TRAVERSORS
      with type ('a, 'e) mon := ('a, 'e) result Lwt.t
      with type ('a, 'e) callermon := ('a, 'e) result Lwt.t
      with type ('a, 'e) t := 'a t

  (* Additional Lwt-option helpers. E.g.,
     [SO.fold_left : ('a -> 'b -> 'a option) -> 'a -> 'b t -> 'a option Lwt.t]
   *)
  module SO :
    Seqes.Sigs.SEQMON1TRAVERSORS
      with type 'a mon := 'a option Lwt.t
      with type 'a callermon := 'a option
      with type 'a t := 'a t
end
= struct
  include Seqes.Monadic.Make1(Lwt)
  (* a thin wrapper to guarantee that exceptions raised in the forcing of the
     first node of a converted sequence becomes a rejected promise *)
  let of_seq s () = Lwt.apply (of_seq s) ()

  (* The Lwt functions are available from the functor as module [M] simply
     renamed here *)
  module S = M

  module E = MakeTraversors2
    (struct
      type ('a, 'e) t = ('a, 'e) Result.t
      let bind = Result.bind
      let return = Result.ok
    end)
    (struct
      type ('a, 'e) t = ('a, 'e) Lwt_result.t
      let bind = Lwt_result.bind
      let return = Lwt_result.return
    end)
    (struct
      let bind x f = match x with
        | Error _ as err -> Lwt.return err
        | Ok x -> f x
    end)
    (struct let bind = Lwt.bind end)

  module ES = MakeTraversors2
    (struct
      type ('a, 'e) t = ('a, 'e) Result.t Lwt.t
      let bind = Lwt_result.bind
      let return = Lwt_result.return
    end)
    (struct
      type ('a, 'e) t = ('a, 'e) Lwt_result.t
      let bind = Lwt_result.bind
      let return = Lwt_result.return
    end)
    (struct
      let bind = Lwt_result.bind
    end)
    (struct let bind = Lwt.bind end)

  (* The Lwt-option functions require some setup with cross-monad binds *)
  module SO =
    MakeTraversors
      (struct
        type 'a t = 'a option
        let return = Option.some
        let bind = Option.bind
      end)
      (struct
        type 'a t = 'a option Lwt.t
        let return = Lwt.return_some
        let bind x f =
          Lwt.bind x (function None -> Lwt.return_none | Some y -> f y)
      end)
      (struct
        let bind x f = match x with None -> Lwt.return_none | Some y -> f y
      end)
      (struct
        let bind x f = Lwt.bind x f
      end)
end

let () =
  Lwt_main.run begin
    List.to_seq [1;1;1]
    |> SeqS.of_seq
    |> SeqS.S.map
        (fun x ->
          let open Lwt.Syntax in
          let* () = Lwt_unix.sleep (float_of_int x) in
          Lwt.return (string_of_int x))
    |> SeqS.S.iter Lwt_io.print
  end

let () = Lwt_main.run (Lwt_io.flush_all ())
let () = print_newline ()

let () =
  let r =
    Lwt_main.run begin
      List.to_seq [1;2;4;1000;20000]
      |> SeqS.of_seq
      |> SeqS.S.map
          (fun x ->
            let open Lwt.Syntax in
            let* () = Lwt_unix.sleep (float_of_int x) in
            Lwt.return (string_of_int x))
      |> SeqS.ES.iteri
          (fun i x ->
            let open Lwt.Syntax in
            let* () = Lwt_io.print x in
            let* () = Lwt_io.flush_all () in
            if i >= 2 then Lwt.return_error "too big" else Lwt.return_ok ())
    end
  in
  match r with
  | Ok () -> assert false (* the input has overflows *)
  | Error msg -> assert (String.equal msg "too big")
