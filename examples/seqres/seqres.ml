(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*                 Raphaël Proust                                         *)
(*                                                                        *)
(*   Copyright 2022 Nomadic Labs                                          *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

module Seq
: sig
  include module type of struct include Stdlib.Seq end

  (* Additional Result traversors *)
  module R : Seqes.Sigs.SEQMON2TRAVERSORS
    with type ('a, 'e) mon := ('a, 'e) result
    with type ('a, 'e) callermon := ('a, 'e) result
    with type ('a, 'e) t := 'a Stdlib.Seq.t
end
= struct
  include Stdlib.Seq
  module R = Seqes.Standard.Make2(struct
    type ('a, 'e) t = ('a, 'e) result
    let return x = Ok x
    let bind = Result.bind
  end)
end

let () =
  let r =
    List.to_seq ["a";"bb";"ccc";"dd";"e";"";"f";"gg"]
    |> Seq.R.iter
        (fun x ->
          if x = "" then Error x else (Stdlib.print_endline x; Ok ()))
  in
  match r with
  | Error "" -> ()
  | _ -> assert false

module SeqR
: sig
  include Seqes.Sigs.SEQMON2ALL
    with type ('a, 'e) mon := ('a, 'e) result

  module R :
    Seqes.Sigs.SEQMON2TRANSFORMERS
      with type ('a, 'e) mon := ('a, 'e) result
      with type ('a, 'e) callermon := ('a, 'e) result
      with type ('a, 'e) t := ('a, 'e) t
end
= struct
  include Seqes.Monadic.Make2(struct
    type ('a, 'e) t = ('a, 'e) result
    let return x = Ok x
    let bind = Result.bind
  end)
  module R = M
end

let () =
  let r =
    List.to_seq ["a";"bb";"ccc";"dd";"e";"";"f";"gg"]
    |> SeqR.of_seq
    |> SeqR.R.mapi
        (fun i x ->
          if x = "" then Error i else Ok (x ^ x ^ x))
    |> SeqR.iter Stdlib.print_endline
  in
  match r with
  | Error 5 -> ()
  | _ -> assert false
