# Seqes

A library to compose the abstraction of the `Stdlib.Seq` module with monads.

The `Stdlib.Seq` module doesn't allow mixing in monads. Specifically, the
`Stdlib.Seq` module provides the following type definition:

```
type 'a t = unit -> 'a node
and +'a node =
  | Nil
  | Cons of 'a * 'a t
```

in which an `'a Seq.t` must be a simple lambda returning a node. You cannot
thread that lambda into a monad.

The `Seqes` library provides functors to generate new `Seq`-like modules,
adapted to your monad.


## `Seqes.Standard`

The functor `Seqes.Standard.Make1` takes a monad parameter.

```
module SeqM = Seqes.Standard.Make1(Monad)
```

It returns a module containing traversors using the provided `Monad`.

```
SeqM.iter : ('a -> unit Monad.t) -> 'a Stdlib.Seq.t -> unit Monad.t
```

Check `examples/seqlwt/seqlwt.ml` for an example use of `Seqes.Standard.Make1`.


## `Seqes.Monadic.Make1`

The functor `Seqes.Monadic.Make1` takes a monad parameter.

```
module SeqM = Seqes.Monadic.Make1(Monad)
```

It returns a module with a fresh type definition integrating the `Seq`
abstraction and the monad together:

```
type 'a t = unit -> 'a node Monad.t
and 'a node =
  | Nil
  | Cons of 'a * 'a t
```

It also exports all the functions from the `Stdlib.Seq` module but operating on
that new type instead.

```
SeqM.iter : ('a -> unit) -> 'a SeqM.t -> unit Monad.t
```

The library provides more advance usage. Specifically, note how the `SeqM.iter`
function takes a function `'a -> unit`, and how it is sometimes useful to
pass a `'a -> unit Monad.t` function instead.

The module produce by `Seqes.Monadic.Make1` actually contains a submodule `M`
with those specialised variants.

```
SeqM.M.iter : ('a -> unit Monad.t) -> 'a SeqM.t -> unit Monad.t
```

In addition to this submodule `M`, the module contains two functors to generate
more specialised sets of functions. See `examples/seqlist/`
for an example of this even more advanced use.

## `Seqes.Standard.Make2` and `Seqes.Monadic.Make2`

If your monad has two type parameters (e.g., `('a, 'e) result`), then you can
use `Seqes.Standard.Make2` and `Seqes.Monadic.Make2` instead of the functors
mentioned above.

See `examples/seqres/seqres.ml` and `examples/seqlwtres/seqlwtres.ml` for
examples.
