The project layout is as follows:

- `lib/` contains the source of the library
- `test/cram/` contains a few cram tests to demonstrate basic capabilities
- `test/pbt/` contains the property-based tests
- `examples/` contains some example instantiation of the library functors

Note that `examples/` is marked as `vendored_dirs` (in the top-level `dune`
file). This is because the examples therein have extra dependencies (e.g., Lwt)
which we do not expect users to have installed on their machine.


# Scope

The scope of the project is

> to provide the necessary tools to combine monads and `Seq`.

From there, the main constraint on contributions:

- Compatibility with `Stdlib.Seq` is a hard requirement

    The `Seqes.Sigs.SEQMON1ALL` signature must be identical to `Stdlib.Seq`
    interface with only the monad added.

    See `examples/seqseq/seqseq.ml`.


# Testing

If you make changes to the code or the test suite, you should run the
mutation-testing framework `mutaml` to ensure the test coverage is complete.

1. Install mutaml: https://github.com/jmid/mutaml

2. Build with instrumentation:

    ```
    DUNE_CACHE=disabled dune build --instrument-with mutaml
    ```

3. Run the tests under the mutator

    a. Edit the `_build/default/mutaml-mut-files.txt` to leave only the entry
       for one of the files you have modified.

    b. Run the test associated to that file under `mutaml-runner`. E.g., for
       `monadic1.ml`, run

        ```
        DUNE_CACHE=disabled QCHECK_LONG=true QCHECK_LONG_FACTOR=50 mutaml-runner "dune exec --no-build test/pbt/test_monadic1.exe"
        ```

    c. Repeat for different files.

    It is somewhat cumbersome to have to fiddle manually with the mutations. One
    welcome contribution would be to improve this and automate some of that
    legwork.


# Formatting

Until the library receives sufficiently many contributions from sufficiently
many authors to justify the use of an automatic code formatter, the code is
formatted by hand.
